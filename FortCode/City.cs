﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode
{
    public class City
    {
        public string CityName { get; set; }
        public string Country { get; set; }
    }
}
