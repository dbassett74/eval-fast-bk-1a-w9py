﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FortCode.Data;
using FortCode.Models;

namespace FortCode.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BarsController : ControllerBase
    {
        private readonly FortCodeContext _context;

        public BarsController(FortCodeContext context)
        {
            _context = context;
        }

        // GET: api/Bars
        [HttpGet]
        public IQueryable<BarResponse> GetBars()
        {
            var bars = from b in _context.Bar
                       select new BarResponse()
                        {
                            Id = b.Id,
                            Name = b.Name
                        };

            return bars;
        }

        // GET: api/Bars/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Bar>> GetBar(int id)
        {
            var bar = await _context.Bar.FindAsync(id);

            if (bar == null)
            {
                return NotFound();
            }

            return bar;
        }

        // PUT: api/Bars/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBar(int id, Bar bar)
        {
            if (id != bar.Id)
            {
                return BadRequest();
            }

            _context.Entry(bar).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BarExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Bars
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Bar>> PostBar(BarPost bar)
        {
            var b = new Bar
            {
                Name = bar.Name,
                City = _context.City.FirstOrDefault(x => x.Id == bar.City)
            };

            _context.Bar.Add(b);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBar", new { id = b.Id });
        }

        // DELETE: api/Bars/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Bar>> DeleteBar(int id)
        {
            var bar = await _context.Bar.FindAsync(id);
            if (bar == null)
            {
                return NotFound();
            }

            _context.Bar.Remove(bar);
            await _context.SaveChangesAsync();

            return bar;
        }

        private bool BarExists(int id)
        {
            return _context.Bar.Any(e => e.Id == id);
        }
    }
}
