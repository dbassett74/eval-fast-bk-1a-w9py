﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FortCode.Data;
using FortCode.Models;

namespace FortCode.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavoriteCitiesController : ControllerBase
    {
        private readonly FortCodeContext _context;

        public FavoriteCitiesController(FortCodeContext context)
        {
            _context = context;
        }

        // GET: api/FavoriteCities
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserCity>>> GetUserCity()
        {
            return await _context.UserCity.ToListAsync();
        }

        // GET: api/FavoriteCities/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserCity>> GetUserCity(int id)
        {
            var userCity = await _context.UserCity.FindAsync(id);

            if (userCity == null)
            {
                return NotFound();
            }

            return userCity;
        }

        // PUT: api/FavoriteCities/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserCity(int id, UserCity userCity)
        {
            if (id != userCity.UserId)
            {
                return BadRequest();
            }

            _context.Entry(userCity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserCityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FavoriteCities
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<UserCity>> PostUserCity(UserCity userCity)
        {
            _context.UserCity.Add(userCity);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserCityExists(userCity.UserId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUserCity", new { id = userCity.UserId }, userCity);
        }

        // DELETE: api/FavoriteCities/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UserCity>> DeleteUserCity(int id)
        {
            var userCity = await _context.UserCity.FindAsync(id);
            if (userCity == null)
            {
                return NotFound();
            }

            _context.UserCity.Remove(userCity);
            await _context.SaveChangesAsync();

            return userCity;
        }

        private bool UserCityExists(int id)
        {
            return _context.UserCity.Any(e => e.UserId == id);
        }
    }
}
