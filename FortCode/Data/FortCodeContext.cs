﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FortCode.Models;

namespace FortCode.Data
{
    public class FortCodeContext : DbContext
    {
        public FortCodeContext (DbContextOptions<FortCodeContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bar>()
              .HasOne(b => b.City)
              .WithMany(c => c.Bars)
              .IsRequired();

            modelBuilder.Entity<User>()
               .HasIndex(u => u.EmailAddress)
               .IsUnique();

            // User - City relationship
            modelBuilder.Entity<UserCity>()
                .HasKey(uc => new { uc.UserId, uc.CityId });
            modelBuilder.Entity<UserCity>()
                .HasOne(uc => uc.User)
                .WithMany(u => u.UserCities)
                .HasForeignKey(uc => uc.UserId);
            modelBuilder.Entity<UserCity>()
                .HasOne(uc => uc.City)
                .WithMany(c => c.UserCities)
                .HasForeignKey(uc => uc.CityId);

            // User - Bar relationship
            modelBuilder.Entity<UserBar>()
                .HasKey(ub => new { ub.UserId, ub.BarId });
            modelBuilder.Entity<UserBar>()
                .HasOne(ub => ub.User)
                .WithMany(u => u.UserBars)
                .HasForeignKey(ub => ub.UserId);
            modelBuilder.Entity<UserBar>()
                .HasOne(ub => ub.Bar)
                .WithMany(b => b.UserBars)
                .HasForeignKey(ub => ub.BarId);
        }

        public DbSet<FortCode.Models.User> User { get; set; }

        public DbSet<FortCode.Models.Bar> Bar { get; set; }

        public DbSet<FortCode.Models.City> City { get; set; }
        public DbSet<FortCode.Models.UserCity> UserCity { get; set; }
    }
}
