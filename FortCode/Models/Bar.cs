﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class Bar
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public City City { get; set; }
        public ICollection<UserBar> UserBars { get; set; }
    }

    public class BarPost
    {
        public string Name { get; set; }
        public int City { get; set; }
    }

    public class BarResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
